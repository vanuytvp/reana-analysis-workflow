def my_function():
    # Your code for the function goes here
    print("Hello, from my_function!")

# Entry point to execute the function
if __name__ == "__main__":
    my_function()
